<?php

namespace App;

use Root\App;

class Model extends App
{
  public function __construct()
  {
    // set model table to querys by class name
    $this->set_model_table();
    // set database connection
    parent::set_Database_Connection();
  }

  /**
   * Set the model default table name in model by parsing Class Name
   * @return void
   */
  public function set_model_table()
  {
    // set table name for queries
    $class_name = explode('App\\', get_class($this))[1];
    return $this->table = strtolower($class_name) . 's';
  }

  /**
   * Generic model query call function
   * @param string $query
   * @return void
   */
  public function make_query(string $query)
  {
    return $this->database->query($query);
  }

  /**
   * Find a model register by id in DB
   * @param int $id
   * @return void
   */
  public function find($id)
  {
    return $this->make_query(
      "SELECT * FROM {$this->table} WHERE {$this->table}.id = {$id};"
    );
  }

  /**
   * Get all the model registers in DB
   * @return void
   */
  public function getAll()
  {
    return $this->make_query(
      "SELECT * FROM {$this->table};"
    );
  }

  /**
   * Make a Query with a Where statement to find in the model DB 
   *
   * @param string $field
   * @param string $value
   * @param string $operator
   * @return void
   */
  public function where(string $field = '', string $value = '', string $operator = '=')
  {
    return $this->make_query(
      "SELECT * FROM {$this->table} WHERE {$this->table}.{$field} {$operator} \"{$value}\";"
    );
  }
}
