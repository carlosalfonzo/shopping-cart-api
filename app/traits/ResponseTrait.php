<?php

namespace App\Traits;

/**
 * JSON Response Trait
 */
trait ResponseTrait
{
  /**
   * Set Response Headers
   *
   * @param int $statusCode
   * @return void
   */
  private static function set_response_headers(int $statusCode)
  {
    // prepare headers to response
    http_response_code($statusCode);
    $headers_status_description = [
      200 => '200 OK',
      400 => '400 Bad Request',
      403 => '403 Forbidden',
      404 => '404 Not Found',
      422 => 'Unprocessable Entity',
      500 => '500 Internal Server Error',
      502 => '502 Bad Gateway'
    ];
    // ok, validation error, or failure
    header_remove('Status');
    header('Status: ' . $headers_status_description[$statusCode]);
    // set the header to make sure cache is forced
    header("Cache-Control: no-cache");
    // treat this as json
    header('Content-Type: application/json');
  }

  /**
   * Send data as Json
   *
   * @param $data
   * @param integer $status
   * @param boolean $database_result
   * @return JsonResponse
   */
  public static function send_data($data, int $status = 200, $database_result = false)
  {
    $response_status = $status;
    self::set_response_headers($response_status);
    // return response as json
    if (is_array($data)) {
      if ($database_result) {
        $data = self::parse_database_result($data);
      }
      echo json_encode(
        array_merge($data, ['status' => $response_status])
      );
      die();
    } else {
      echo json_encode([
        'data' => $data,
        'status' => $response_status
      ]);
    }
    die();
  }

  /**
   * Send error message as Json
   *
   * @param string $error_message
   * @param integer $status
   * @param string $trace
   * @return void
   */
  public static function send_error(string $error_message, int $status = 400, $trace = '')
  {
    $response_status = $status;
    self::set_response_headers($response_status);
    if ($trace !== '') {
      // return response as json
      echo json_encode([
        'message' => $error_message,
        'trace' => $trace,
        'error' => true,
        'status' => $response_status
      ]);
      die();
    }
    // return response as json
    echo json_encode([
      'message' => $error_message,
      'error' => true,
      'status' => $response_status
    ]);
    die();
  }

  public static function parse_database_result($data, $with_data_key = true)
  {
    $parsed = [];
    // format database result to be frontend friendly
    for ($i = 0; $i < count($data); $i++) {
      $parsed[(string) $data[$i]['id']] = $data[$i];
    }
    if (!$with_data_key) {
      $parsed_result = $parsed;
    } else {
      $parsed_result['data'] = $parsed;
    }
    return $parsed_result;
  }
}
