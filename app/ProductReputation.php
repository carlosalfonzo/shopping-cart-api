<?php

namespace App;

use App\Model;

class ProductReputation extends Model
{
  protected $fields = [
    'user_id',
    'product_id',
    'reputation'
  ];

  public function __construct()
  {
    parent::__construct();
    $this->table = 'product_reputation';
  }

  /**
   * query the average reputation for specific product
   *
   * @param integer $product_id
   * @return array
   */
  public function get_product_reputation(int $product_id)
  {
    return $this->make_query(
      "SELECT FORMAT(AVG({$this->table}.reputation), 1) as product_reputation
        FROM {$this->table}
        WHERE {$this->table}.product_id = {$product_id};"
    )[0];
  }

  /**
   * validate if user has any rate for this product
   *
   * @param integer $product_id
   * @param integer $user_id
   * @return boolean
   */
  public function user_can_rate_product(int $product_id, int $user_id)
  {
    return count(
      $this->make_query(
        "SELECT id FROM {$this->table}
          WHERE {$this->table}.user_id = {$user_id}
          AND {$this->table}.product_id = {$product_id};"
      )
    ) === 0;
  }

  /**
   * create new product reputation
   *
   * @param array $query_params
   * @return boolean
   */
  public function set_product_reputation(array $query_params)
  {
    return $this->make_query(
      "INSERT INTO {$this->table}(user_id, product_id, reputation) 
      VALUES (
        {$query_params['user_id']},
        {$query_params['product_id']},
        {$query_params['reputation']}
      );"
    );
  }
}
