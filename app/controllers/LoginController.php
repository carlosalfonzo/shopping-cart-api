<?php

namespace App\Controllers;

use App\Controllers\Controller;
use App\Request;
use App\User;
use App\Auth;

class LoginController extends Controller
{
  /**
   * Login in API
   *
   * @param Request $request
   * @return JsonResponse
   */
  public function login(Request $request)
  {
    // validating if all the required fields are in request
    if (!isset($request->username) || !isset($request->password)) {
      self::send_error('Missing username or password');
    }
    // find the user by username
    $user = new User();
    $user = $user->where('username', $request->username);
    if (!count($user)) {
      self::send_error('User not found');
    }
    $user = $user[0]; //select the first element in collection
    // validate login attempt
    $auth = new Auth();
    if (!$auth->validate_login_attempt($user['password'], $request->password)) {
      self::send_error('Invalid username and password combination');
    }
    // create user token
    $token = $auth->assign_token((int) $user['id']);
    // hide password to response
    unset($user['password']);
    // send success response with user and token
    return self::send_data([
      'user' => $user,
      'token' => 'Bearer ' . $token
    ]);
  }

  /**
   * Logout App (reject Token)
   *
   * @param Request $request
   * @return JsonResponse
   */
  public function logout(Request $request)
  {
    $auth = new Auth();
    $auth->reject_token($request::get_token());
    return self::send_data('Loged out');
  }

  /**
   * Create a new user in API
   *
   * @param Request $request
   * @return JsonResponse
   */
  public function sign_up(Request $request)
  {
    // validating if all the required fields are in request
    if (!isset($request->username) || !isset($request->password)) {
      self::send_error('Missing username or password');
    }
    $user = new User();
    // validating if the username is available
    if (count($user->where('username', $request->username)) > 0) {
      self::send_error('Already taken username, chose new one');
    }
    // create user
    $user->create_new_user($request);
    self::send_data([
      'message' => 'Successfully user created',
    ]);
  }
}
