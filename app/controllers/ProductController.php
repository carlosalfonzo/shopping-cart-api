<?php

namespace App\Controllers;

use App\Controllers\Controller;
use App\Request;
use App\Product;

class ProductController extends Controller
{

  /**
   * Get all API products
   *
   * @param Request $request
   * @return JsonResponse
   */
  public function index(Request $request)
  {
    $products = new Product();
    self::send_data($products->getAll(), 200, true);
  }
}
