<?php

namespace App\Controllers;

use App\Traits\ResponseTrait;

/**
 * Generic Controller Class 
 *
 * @param string $model
 */
class Controller
{
  use ResponseTrait;

  /**
   * Creates Model instance
   *
   * @param string $model
   */
  public function __construct(string $model)
  {
    $this->set_Model($model);
  }

  /**
   * Set the model name to controller
   *
   * @param [type] $model_name
   * @return void
   */
  public function set_Model($model_name)
  {
    $this->model = $model_name;
  }
}
