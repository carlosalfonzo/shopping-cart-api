<?php

namespace App\Controllers;

use App\Controllers\Controller;
use App\Request;
use App\Cart;

class CartController extends Controller
{

  /**
   * Show the user cart
   *
   * @param Request $request
   * @return JsonResponse
   */
  public function showCart(Request $request)
  {
    $cart = new Cart();
    $user_active_carts = $cart->user_has_active_carts($request->user()['id']);
    if ($user_active_carts) {
      $user_cart = $cart->get_active_cart_with_products($request->user()['id']);
    } else {
      $cart->create_new_cart($request->user()['id']);
      $user_cart = $cart->get_active_cart_with_products($request->user()['id']);
    }
    self::send_data([$user_cart], 200);
  }

  /**
   * Add, Remove or update products in active user Cart
   *
   * @param Request $request
   * @return JsonResponse
   */
  public function addOrRemoveItems(Request $request)
  {
    // get user active cart
    $cart = new Cart();
    $user_cart = $cart->get_active_user_carts($request->user()['id'])[0];
    if (isset($request->products_to_add)) {
      $cart->add_products_to_cart($request->products_to_add, $user_cart['id']);
      self::send_data('Successfully added product(s)');
    }
    if (isset($request->products_to_update_quantity)) {
      $cart->update_product_quantity($request->products_to_update_quantity, $user_cart['id']);
      self::send_data('Successfully updated product(s)');
    }
    if (isset($request->products_to_remove)) {
      $cart->remove_products_to_cart($request->products_to_remove, $user_cart['id']);
      self::send_data('Successfully removed product(s)');
    }
  }
}
