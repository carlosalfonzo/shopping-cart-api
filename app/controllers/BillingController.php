<?php

namespace App\Controllers;

use App\Controllers\Controller;
use App\Request;
use App\Billing;
use App\User;
use App\Cart;

class BillingController extends Controller
{

  /**
   * Pay a active cart
   *
   * @param Request $request
   * @return JsonResponse
   */
  public function pay(Request $request)
  {
    // check if it's selected a shipping method and if is a valid one
    if (!isset($request->shipping) || !isset(Billing::SHIPPPING_METHOD[$request->shipping])) {
      self::send_error('Invalid Shipping method');
    }

    $request_user = $request->user();
    $cart = new Cart();
    // check if user has an active cart
    if (!$cart->user_has_active_carts($request_user['id'])) {
      self::send_error('User has no active cart');
    }

    // select active user cart 
    $active_cart_id = $cart->get_active_user_carts($request_user['id'])[0]['id'];

    // check if user active cart has products added
    if (count($cart->get_active_cart_with_products($request_user['id'])['products']) === 0) {
      self::send_error('User active cart has no product added, add at least one before go to billing process');
    }

    // calculate the cart total adding the billing method price to operation
    $billing_total = $cart->get_cart_total($active_cart_id, Billing::SHIPPPING_METHOD_PRICE[$request->shipping]);

    // check if user has founds
    if ((float) $request_user['balance'] < (float) $billing_total) {
      self::send_error('User has no founds');
    }

    // create new user billing
    $bill = new Billing();
    $bill->create_new_bill([
      'user_id' => $request_user['id'],
      'total' => $billing_total,
      'cart_id' => $active_cart_id,
      'shipping' => intval($request->shipping) ? intval($request->shipping) : Billing::SHIPPPING_METHOD[$request->shipping]
    ]);

    // deduct billing total from user balance
    $user = new User();
    $new_user_balance = $user->deduct_billing_total($request_user['id'], $billing_total);

    // set cart status to paid
    $cart->set_cart_as_paid($active_cart_id);

    return self::send_data([
      'new_user_balance' => $new_user_balance,
      'message' => 'Billing process successfully done',
    ]);
  }

  /**
   * Get checkout info
   *
   * @param Request $request
   * @return JsonResponse
   */
  public function checkOutInfo(Request $request)
  {
    // check if it's selected a shipping method and if is a valid one
    if (!isset($request->shipping) || !isset(Billing::SHIPPPING_METHOD[$request->shipping])) {
      self::send_error('Invalid Shipping method');
    }

    $request_user = $request->user();
    $cart = new Cart();
    // check if user has an active cart
    if (!$cart->user_has_active_carts($request_user['id'])) {
      self::send_error('User has no active cart');
    }

    // select active user cart 
    $active_cart_id = $cart->get_active_user_carts($request_user['id'])[0]['id'];

    // check if user active cart has products added
    if (count($cart->get_active_cart_with_products($request_user['id'])['products']) === 0) {
      self::send_error('User active cart has no product added, add at least one before go to billing process');
    }
    // calculate the cart total adding the billing method price to operation
    $info = $cart->get_checkout_info($active_cart_id, Billing::SHIPPPING_METHOD_PRICE[$request->shipping]);

    return self::send_data($info);
  }

  /**
   * Get all user billings
   *
   * @param Request $request
   * @return JsonResponse
   */
  public function userBillings(Request $request)
  {
    $billings = new Billing();
    return self::send_data($billings->where('user_id', $request->user()['id']), 200, true);
  }
}
