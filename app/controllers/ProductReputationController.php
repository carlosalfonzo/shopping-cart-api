<?php

namespace App\Controllers;

use App\Controllers\Controller;
use App\Product;
use App\Request;
use App\ProductReputation;

class ProductReputationController extends Controller
{
  /**
   * Get product average reputation
   *
   * @param Request $request
   * @return JsonResponse
   */
  public function show(Request $request)
  {
    // checking if Request has product id
    if (!isset($request->product_id)) {
      self::send_error('Missing product Id');
    }
    // checking if product is in database
    if (!count((new Product())->find($request->product_id))) {
      self::send_error('Product not found');
    }
    // get product reputation
    $product_reputation = new ProductReputation();
    self::send_data(
      $product_reputation->get_product_reputation($request->product_id)
    );
  }

  /**
   * Save a user product rate
   *
   * @param Request $request
   * @return JsonResponse
   */
  public function store(Request $request)
  {
    // checking if Request has product id and reputation
    if (!isset($request->product_id) || !isset($request->reputation)) {
      self::send_error('Missing product Id or Reputation');
    }
    // checking if product is in database
    if (!count((new Product())->find($request->product_id))) {
      self::send_error('Product not found');
    }
    // checking if is valid reputation (parsing only integer)
    $request->reputation = (int) $request->reputation;
    if ($request->reputation > 5 || $request->reputation <= 0) {
      self::send_error('Invalid product reputation, reputation has to be integer value between 1 and 5');
    }
    $product_reputation = new ProductReputation();
    $user = $request->user();
    // check if user have any reputation to that product
    if (!$product_reputation->user_can_rate_product($request->product_id, $user['id'])) {
      self::send_error("User {$user['username']}, already rate this product");
    }
    // set product reputation
    $product_reputation->set_product_reputation(
      [
        'user_id' => $user['id'],
        'product_id' => $request->product_id,
        'reputation' => $request->reputation
      ]
    );
    self::send_data(['message' => 'Thank you for rating this product']);
  }
}
