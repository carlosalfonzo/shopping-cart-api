<?php

namespace App;

use App\Model;

class Product extends Model
{
  protected $fields = [
    'title',
    'price',
    'image'
  ];
}
