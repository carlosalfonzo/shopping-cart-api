<?php

namespace App;

use App\Model;

class User extends Model
{
  protected $fields = [
    'username',
    'password',
    'balance'
  ];

  /**
   * Stores a new User in DB
   *
   * @param Request $request
   * @return boolean
   */
  public function create_new_user(Request $request)
  {
    // save secure password
    $request->password = password_hash($request->password, PASSWORD_DEFAULT);
    // run insert query
    return $this->make_query(
      "INSERT INTO {$this->table}(username, password, balance) 
      VALUES (
        \"{$request->username}\",
        \"{$request->password}\",
        100
      );"
    );
  }

  /**
   * Deduct billing total from User's Balance
   *
   * @param integer $user_id
   * @param $total_to_deduct
   * @return void
   */
  public function deduct_billing_total(int $user_id, $total_to_deduct)
  {
    $this->make_query(
      "UPDATE {$this->table}
      SET {$this->table}.balance = {$this->table}.balance - {$total_to_deduct}
      WHERE {$this->table}.id = {$user_id};"
    );
    return $this->find($user_id)[0]['balance'];
  }
}
