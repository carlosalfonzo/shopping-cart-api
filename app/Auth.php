<?php

namespace App;

use App\Model;

/**
 * API Authentication Model
 */
class Auth extends Model
{
  protected $fields = [
    'user_id',
    'token',
    'rejected'
  ];

  public function __construct()
  {
    parent::__construct();
    $this->table = 'auth_tokens';
  }

  /**
   * Assign new token to user
   *
   * @param integer $user_id
   * @return string
   */
  public function assign_token(int $user_id)
  {
    // create new token to user
    $token = $this->genetate_auth_token();
    // run insert query
    $this->make_query(
      "INSERT INTO {$this->table}(user_id, token, rejected) 
      VALUES (
        {$user_id},
        \"{$token}\",
        false
      );"
    );
    return $token;
  }

  /**
   * Reject user token
   *
   * @param string $token
   * @return void
   */
  public function reject_token(string $token)
  {
    $this->make_query(
      "UPDATE {$this->table}
      SET {$this->table}.rejected = true
      WHERE {$this->table}.token = \"{$token}\""
    );
  }

  /**
   * Check if request header token is valid
   *
   * @param string $haeder_token
   * @return boolean
   */
  public static function is_valid_token(string $header_token)
  {
    // check if token exists
    $token = new self();
    $token = $token->where('token', $header_token);
    if (!count($token)) return false;
    // check if token is not rejected
    if ($token[0]['rejected']) return false;
    // is valid token
    return true;
  }

  /**
   * Generate API Auth Token
   *
   * @return string
   */
  private function genetate_auth_token()
  {
    // generate valid api auth token by hashing and encoding base 64
    return base64_encode(hash("sha256", time()));
  }

  /**
   * Validate user Login Attempt
   *
   * @param string $user_stored_password
   * @param string $password
   * @return boolean
   */
  public function validate_login_attempt(string $user_stored_password, string $password)
  {
    // find stored password hash and validate it vs request password
    return password_verify($password, $user_stored_password);
  }
}
