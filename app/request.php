<?php

namespace App;

use App\Auth;
use App\User;

class Request
{
  /**
   * Create a Request instance
   *
   * @param string $request_method
   */
  public function __construct($method)
  {
    $this->method = $method;
    $this->generate_request_instance();
  }

  /**
   * set as class attribute, all the request data
   *
   * @return void
   */
  private function generate_request_instance()
  {
    $raw_request = [];
    if ($this->method === 'GET') {
      $raw_request = $_GET;
    }
    if ($this->method === 'POST') {
      $raw_request = json_decode(file_get_contents("php://input"));
    }
    foreach ($raw_request as $request_param => $request_value) {
      if (isset($request_value)) $this->$request_param = $request_value;
    }
  }

  /**
   * Returns all present request elements
   *
   * @return array
   */
  public function all()
  {
    $all_values = [];
    foreach (get_object_vars($this) as $attribute => $value) {
      if ($attribute !== 'method') $all_values[$attribute] = $value;
    }
    return $all_values;
  }

  /**
   * Get Authentication token from header
   *
   * @return string
   */
  public static function get_token()
  {
    $headers = getallheaders();
    return explode('Bearer ', $headers['Authorization'])[1];
  }

  /**
   * Get user loged in API by present Auth token in request
   *
   * @return array
   */
  public function user()
  {
    $token = (new Auth())->where('token', self::get_token())[0];
    return (new User())->find($token['user_id'])[0];
  }
}
