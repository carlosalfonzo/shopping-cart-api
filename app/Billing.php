<?php

namespace App;

use App\Model;

class Billing extends Model
{
  protected $fields = [
    'user_id',
    'cart_id',
    'total',
    'shipping'
  ];

  // shipping valid values
  public const SHIPPPING_METHOD_PRICE = [
    1 => 5,
    'UPS' => 5,
    2 => 2,
    'Pick Up' => 2,
  ];

  // shipping values meaning
  public const SHIPPPING_METHOD = [
    1 => 'UPS',
    'UPS' => 1,
    2 => 'Pick Up',
    'Pick Up' => 2,
  ];

  /**
   * Stores a new Billing in DB
   *
   * @param array $query_args
   * @return void
   */
  public function create_new_bill(array $query_args)
  {
    return $this->make_query(
      "INSERT INTO {$this->table}(user_id, total, shipping, cart_id) 
      VALUES (
        {$query_args['user_id']},
        {$query_args['total']},
        {$query_args['shipping']},
        {$query_args['cart_id']}
      );"
    );
  }
}
