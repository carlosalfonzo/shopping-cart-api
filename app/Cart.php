<?php

namespace App;

use App\Model;
use App\Product;
use App\Traits\ResponseTrait;

class Cart extends Model
{
  protected $fields = [
    'status',
    'user_id',
  ];

  // valid cart status values meaning
  public const STATUS = [
    0 => 'Unpaid Cart',
    'Unpaid Cart' => 0,
    1 => 'Paid Cart',
    'Paid Cart' => 1,
  ];

  /**
   * check if user has a active cart
   *
   * @param integer $user_id
   * @return boolean
   */
  public function user_has_active_carts(int $user_id)
  {
    $carts = $this->get_active_user_carts($user_id);
    return count($carts) > 0;
  }

  /**
   * run insert query for model
   *
   * @param integer $user_id
   * @return boolean
   */
  public function create_new_cart(int $user_id)
  {
    $status = self::STATUS['Unpaid Cart'];
    return $this->make_query(
      "INSERT INTO {$this->table}(status,user_id) 
      VALUES (
        {$status},
        {$user_id}
      );"
    );
  }

  /**
   * get all the active user carts, and order by the most recent to older
   *
   * @param [type] $user_id
   * @return array
   */
  public function get_active_user_carts($user_id)
  {
    return $this->make_query(
      "SELECT * FROM {$this->table} 
      WHERE {$this->table}.user_id = $user_id 
      AND {$this->table}.status = 0
      ORDER BY {$this->table}.id DESC;"
    );
  }

  /**
   * Get cart total, by adding all the cart's products and shipping method price
   *
   * @param integer $cart_id
   * @param $shipping
   * @return void
   */
  public function get_cart_total(int $cart_id, $shipping_value)
  {
    return $this->make_query(
      "SELECT FORMAT(SUM(carts_in_products_totals.product_total) + {$shipping_value}, 4) as cart_total
      FROM (
          SELECT products.price*cart_products.quantity as product_total 
          FROM {$this->table}
          INNER JOIN cart_products ON {$this->table}.id = cart_products.cart_id
          INNER JOIN products ON cart_products.product_id = products.id
          WHERE {$this->table}.id = {$cart_id}
      ) AS carts_in_products_totals"
    )[0]['cart_total'];
  }

  /**
   * Update cart status to paid 
   *
   * @param integer $cart_id
   * @return void
   */
  public function set_cart_as_paid(int $cart_id)
  {
    return $this->make_query(
      "UPDATE {$this->table}
      SET {$this->table}.status = 1
      WHERE {$this->table}.id = {$cart_id};"
    );
  }

  /**
   * get active user cart with products
   *
   * @param integer $user_id
   * @return array
   */
  public function get_active_cart_with_products(int $user_id)
  {
    // get cart 
    $cart_data = $this->get_active_user_carts($user_id)[0];
    // get cart product
    $raw_cart_products = $this->make_query(
      "SELECT products.id as id, products.title as title , products.price, products.image, cart_products.quantity as quantity
      FROM {$this->table}
      INNER JOIN cart_products ON {$this->table}.id = cart_products.cart_id
      INNER JOIN products ON cart_products.product_id = products.id
      WHERE {$this->table}.id = {$cart_data['id']};"
    );
    // format cart product to frontend friendly json
    $cart_data['products'] = ResponseTrait::parse_database_result($raw_cart_products, false);
    return $cart_data;
  }

  /**
   * add products to cart
   *
   * @param array $products_array
   * @param integer $cart_id
   * @return void
   */
  public function add_products_to_cart(array $products_array, int $cart_id)
  {
    foreach ($products_array as $request_product) {
      $product = new Product();
      $product = $product->find($request_product->id)[0];
      $this->make_query(
        "INSERT INTO cart_products(product_id, cart_id, quantity) 
        VALUES (
          {$product['id']},
          {$cart_id},
          {$request_product->quantity}
        );"
      );
    }
  }

  /**
   * update cart product quantity
   *
   * @param array $products_array
   * @param integer $cart_id
   * @return void
   */
  public function update_product_quantity(array $products_array, int $cart_id)
  {
    foreach ($products_array as $request_product) {
      // check if product is in cart to update it, if not, does nothing
      if ($this->cart_has_product($cart_id, $request_product->id)) {
        // set new product quantity
        $quantity = (int) $request_product->quantity ?? 1;
        $this->make_query(
          "UPDATE cart_products
          SET cart_products.quantity = {$quantity}
          WHERE cart_products.cart_id = {$cart_id}
          AND cart_products.product_id = {$request_product->id};"
        );
      }
    }
  }

  /**
   * check if product is in cart
   *
   * @param integer $cart_id
   * @param integer $product_id
   * @return boolean
   */
  public function cart_has_product(int $cart_id, int $product_id)
  {
    return count(
      $this->make_query(
        "SELECT * FROM cart_products 
        WHERE cart_products.cart_id = {$cart_id}
        AND cart_products.product_id = {$product_id};"
      )
    ) > 0;
  }

  /**
   * remove cart products
   *
   * @param array $products_id_array
   * @param integer $cart_id
   * @return void
   */
  public function remove_products_to_cart(array $products_id_array, int $cart_id)
  {
    foreach ($products_id_array as $product_id) {
      // check if product is in cart to delete it from, if not, does nothing
      if ($this->cart_has_product($cart_id, $product_id)) {
        $this->make_query(
          "DELETE FROM cart_products
          WHERE cart_products.cart_id = {$cart_id}
          AND cart_products.product_id = {$product_id};"
        );
      }
    }
  }

  public function get_checkout_info(int $cart_id, $shipping_value)
  {
    // get carts totals
    $cartsTotals = $this->make_query(
      "SELECT 
      FORMAT(SUM(carts_in_products_totals.product_total) + {$shipping_value}, 3) as cart_total, 
      FORMAT(SUM(carts_in_products_totals.product_total), 3) as subtotal
      FROM (
          SELECT products.price*cart_products.quantity as product_total 
          FROM {$this->table}
          INNER JOIN cart_products ON {$this->table}.id = cart_products.cart_id
          INNER JOIN products ON cart_products.product_id = products.id
          WHERE {$this->table}.id = {$cart_id}
      ) AS carts_in_products_totals"
    )[0];

    // get new user balance
    $user_balance = $this->make_query(
      "SELECT
      FORMAT( users.balance - {$cartsTotals['cart_total']} , 3) as new_user_balance,
      FORMAT(users.balance, 3 ) as old_user_balance
      FROM {$this->table}
      INNER JOIN users ON {$this->table}.user_id = users.id
      WHERE {$this->table}.id = {$cart_id};"
    )[0];

    return array_merge($cartsTotals, $user_balance);
  }
}
