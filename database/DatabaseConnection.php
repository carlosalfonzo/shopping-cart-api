<?php

namespace Database;

use App\Traits\ResponseTrait;
use mysqli;

/**
 * API DataBase Connection Class
 */
class DatabaseConnection
{
  use ResponseTrait;

  /**
   * Get params to stablish connection with database
   *
   * @param $server_name
   * @param $username
   * @param $password
   * @param $db_name
   * @param $port
   */
  public function __construct(
    $server_name,
    $username,
    $password,
    $db_name,
    $port
  ) {
    // class variables for database connection
    $this->host = $server_name ?? 'localhost';
    $this->username = $username ?? 'root';
    $this->password = $password ?? '';
    $this->database = $db_name ?? '';
    $this->port = $port ?? 3306;
  }

  /**
   * Attempt to connect to Database
   *
   * @return void
   */
  public function connect()
  {
    $this->mySql = new mysqli(
      $this->host,
      $this->username,
      $this->password,
      $this->database,
      $this->port
    );
    if ($this->mySql->connect_errno) {
      self::send_error('Database connection error', 500);
    }
    return $this;
  }

  /**
   * Run Query in database
   *
   * @param string $query
   * @return void
   */
  public function query(string $query)
  {
    $query = $this->mySql->query($query);
    return $this->parse_query_result($query);
  }

  /**
   * Parse Database Result to readable array or boolean value
   *
   * @param $result
   * @return void
   */
  private function parse_query_result($result)
  {
    if (gettype($result) === 'boolean') {
      return $this->parse_bool_result($result);
    }
    return $this->parse_rows_result($result);
  }

  /**
   * Parse to array a query row result
   *
   * @param $result
   * @return array
   */
  private function parse_rows_result($result)
  {
    $parsed_result = [];
    if ($result->num_rows > 0) {
      for ($i = 0; $i < $result->num_rows; $i++) {
        $result->data_seek($i);
        $parsed_row = $result->fetch_assoc();
        $parsed_result[$i] = $parsed_row;
      }
    }
    return $parsed_result;
  }

  /**
   * Parse to bool a query bool result, if false, throw error
   *
   * @param $result
   * @return boolean
   */
  private function parse_bool_result($result)
  {
    if (!$result) {
      self::send_error('Database error', 500, "MYSQL ERROR #{$this->mySql->errno} {$this->mySql->error}");
    }
    return $result;
  }
}
