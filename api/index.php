<?php
require __DIR__ . '\..\vendor\autoload.php';

use Root\App;

/**
 * Initialitiate Shopping Cart API
 */
new App();
