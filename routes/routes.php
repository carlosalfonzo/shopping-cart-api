<?php

/**
 * Setting API Valid Enpoints
 * @example 'endpoint' => [ 'method' => 'request_method', 'controller' => 'controller@function', 'auth' => false ]
 */
const API_ENPOINTS = [
  // Auth Controller Urls
  'login' => [
    'method' => 'POST',
    'controller' => 'LoginController@login',
    'auth' => false,
  ],
  'signup' => [
    'method' => 'POST',
    'controller' => 'LoginController@sign_up',
    'auth' => false,
  ],
  'logout' => [
    'method' => 'GET',
    'controller' => 'LoginController@logout',
    'auth' => true,
  ],
  // Product Controller Urls
  'products' => [
    'method' => 'GET',
    'controller' => 'ProductController@index',
    'auth' => true,
  ],
  // Cart Controller Urls
  'my-cart' => [
    'method' => 'GET',
    'controller' => 'CartController@showCart',
    'auth' => true,
  ],
  'add-or-remove-item-to-cart' => [
    'method' => 'POST',
    'controller' => 'CartController@addOrRemoveItems',
    'auth' => true,
  ],
  // Product Reputation Controller Urls
  'product-reputation' => [
    'method' => 'GET',
    'controller' => 'ProductReputationController@show',
    'auth' => true,
  ],
  'send-product-reputation' => [
    'method' => 'POST',
    'controller' => 'ProductReputationController@store',
    'auth' => true,
  ],
  // Billing Controller Urls
  'check-out' => [
    'method' => 'GET',
    'controller' => 'BillingController@checkOutInfo',
    'auth' => true,
  ],
  'pay' => [
    'method' => 'POST',
    'controller' => 'BillingController@pay',
    'auth' => true,
  ],
  'my-billings' => [
    'method' => 'GET',
    'controller' => 'BillingController@userBillings',
    'auth' => true,
  ],
];
