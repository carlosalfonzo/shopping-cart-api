<?php

namespace Routes;

include_once('routes.php');

use App\Traits\ResponseTrait;
use App\Auth;
use App\Request;

class Endpoints
{
  use ResponseTrait;

  /**
   * Creates a API Endpoint instance
   */
  public function __construct()
  {
    $uri = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
    $this->raw_uri =  strtolower(explode('/api/', $uri)[1]);
    $this->request_method = $_SERVER["REQUEST_METHOD"];
  }

  /**
   * Check the Current API Endpoint and validates it matchs with the corresponding Enpoint configuration
   *
   * @return void
   */
  public function check_api_endpoint()
  {
    if ($this->request_method === 'OPTIONS') {
      echo '';
      die();
    }
    // check if the endpoint is registered in the routes array 
    if (!array_key_exists($this->raw_uri, API_ENPOINTS) || API_ENPOINTS[$this->raw_uri]['method'] !== $this->request_method) {
      // if the route is not matched or is not the correct method, returns 404
      self::send_error('Not Found Url', 404);
    }
    $this->endpoint_args = API_ENPOINTS[$this->raw_uri];
    // check if the route is protected and velidate Authorization request 
    if ($this->endpoint_args['auth']) {
      if (!array_key_exists('Authorization', getallheaders())) {
        self::send_error('Unauthenticated user', 403);
      }
      if (!Auth::is_valid_token(Request::get_token())) {
        self::send_error('Unauthenticated user', 403);
      }
    }
  }
}
