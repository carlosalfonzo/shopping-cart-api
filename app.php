<?php

namespace Root;

use Database\DatabaseConnection;
use Routes\Endpoints;
use Dotenv\Dotenv;
use App\Request;

// set enviroment variable
$environment = (new Dotenv(__DIR__))->load();

class App
{
  public function __construct()
  {
    // set CORS headers
    $this->set_Server_Cors();
    // check api call endpoint
    $this->check_endpoint();
    // open controller
    $this->open_Controller();
  }

  /**
   * Creates a Database Connection instance and try to connect the DB
   *
   * @return void
   */
  public function set_Database_Connection()
  {
    //set database connection
    $database = new DatabaseConnection(
      getenv('HOST') ?? null,
      getenv('DB_USER') ?? null,
      getenv('PASSWORD') ?? null,
      getenv('DATABASE') ?? null,
      getenv('DB_PORT') ?? null,
    );
    $this->database = $database->connect();
  }

  /**
   * Create a API Enpoints intance to check the current API endpoint
   *
   * @return void
   */
  private function check_endpoint()
  {
    $this->appUrl = new Endpoints();
    $this->appUrl->check_api_endpoint();
  }

  /**
   * Takes the valid API Endpoint and parse the API Endpoint config args to creat a instance of the calling Controller
   *
   * @return void
   */
  private function open_Controller()
  {
    // get controller class Name and file
    $controller_url_explode = explode('@', $this->appUrl->endpoint_args['controller']);
    $controller_name = $controller_url_explode[0];
    $model_name = explode("Controller", $controller_name)[0];
    $controller_function = $controller_url_explode[1];
    // create controller valid class name
    $controller_model = 'App\Controllers\\' . $controller_name;
    // create controller instance and call function
    $this->controller = new $controller_model($model_name);
    $this->controller->{$controller_function}(
      new Request($this->appUrl->request_method)
    );
  }

  /**
   * Set the API CORS config
   *
   * @return void
   */
  private function set_Server_Cors()
  {
    // set cors configuration to API
    header('Access-Control-Allow-Origin: *');
    header('Access-Control-Allow-Headers: *');
    header('Access-Control-Allow-Credentials: true');
    header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS");
  }
}
